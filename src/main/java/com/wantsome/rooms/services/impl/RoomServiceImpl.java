package com.wantsome.rooms.services.impl;

import com.wantsome.rooms.model.Room;
import com.wantsome.rooms.repository.RoomRepository;
import com.wantsome.rooms.services.RoomService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RoomServiceImpl implements RoomService {

    private static List<Room> roomList = new ArrayList<>();

    @Autowired
    RoomRepository roomRepository;

    @Override
    public String hello() {
        return "Hello World";
    }

//    OLD VERSION CODE - no mySQL
//    @Override
//    public Room addRoom(Room room) {
//        room.setId(UUID.randomUUID().toString());
//        roomList.add(room);
//        return room;
//    }

    @Override
    public Room addRoom(Room room) {
        room.setId(UUID.randomUUID().toString());
        roomList.add(room);
        roomRepository.save(room);
        return room;
    }

    @Override
    public List<Room> addMultipleRooms(List<Room> roomListToAdd) {
        for (Room room : roomListToAdd) {
            room.setId(UUID.randomUUID().toString());
            roomList.add(room);
        }
        roomRepository.saveAll(roomList);
        return roomList;
    }

//      OLD version - no MySql (read/write into a list)
//    @Override
//    public Optional<Room> getRoomId(String id) {
//        try {
//            return roomList.stream().filter(s -> s.getId().equals(id)).findAny();
//        } catch (Exception e) {
//            System.err.println("Room id " + id + " not found");
//        }
//        return null;
//    }

    @Override
    public Room getRoomId(String id) {
        Optional<Room> room = roomRepository.findById(id);
        if (room.isPresent()) {
            return room.get();
        }
        throw new RuntimeException(String.format("Room with id %d not found", id));
    }

//      OLD version - no MySql (read/write into a list)
//    @Override
//    public List<Room> getAllRoom() {
//        return roomList;
//    }

    @Override
    public Iterable<Room> getAllRoom() {
        return roomRepository.findAll();
    }

//    @Override
//    public Optional<Room> updateRoom(Room room) {
//        Optional<Room> roomToUpdate = roomList.stream().filter(s -> s.getId().equals(room.getId())).findAny();
//        if (roomToUpdate.isPresent()) {
//            Room roomFromList = roomToUpdate.get();
//            if (!roomFromList.equals(room)) {
//                roomFromList.setCapacity(room.getCapacity());
//                roomFromList.setFloor(room.getFloor());
//                roomFromList.setName(room.getName());
//                roomFromList.setDescription(room.getDescription());
//            }
//        }
//        return roomToUpdate;
//    }


//    @Override
//    public Optional<Room> updateRoom(Room room) {
//        Optional<Room> roomToUpdate =
//                roomList.stream()
//                        .filter(s -> s.getId().equals(room.getId()))
//                        .findAny();
//        roomToUpdate.ifPresent(room1 -> BeanUtils.copyProperties(room, room1));
//        return roomToUpdate;
//    }


    @Override
    public Optional<Room> updateRoom(Room room) {
        Optional<Room> roomToUpdate = roomRepository.findById(room.getId());
        roomToUpdate.ifPresent(room1 -> BeanUtils.copyProperties(room, room1));
        return roomToUpdate;
    }


//      OLD version - no MySql (read/write into a list)
//    // https://www.baeldung.com/java-collection-remove-elements
//    @Override
//    public void deleteRoomById(String id) {
//        try {
//            roomList.removeIf(e -> e.getId().equals(id));
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//    }


    // https://www.baeldung.com/java-collection-remove-elements
    @Override
    public void deleteRoomById(String id) {
        Optional<Room> room = roomRepository.findById(id);
        if (room.isPresent()) {
            roomRepository.deleteById(id);
        }
        throw new RuntimeException(String.format("Room with id %d not found", id));
    }
}
