package com.wantsome.rooms;

import com.wantsome.rooms.controllers.RoomController;
import com.wantsome.rooms.model.Room;
import com.wantsome.rooms.services.RoomService;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

// LINK: https://www.baeldung.com/spring-boot-testing
// https://spring.io/guides/gs/testing-web/

/* @RunWith(SpringRunner.class) is used to provide a bridge between Spring Boot test features and JUnit.
Whenever we are using any Spring Boot testing features in our JUnit tests, this annotation will be required.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RoomsApplicationTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private RoomController roomController;

    @MockBean
    private RoomService roomService;

//    @MockBean
//    private RoomRepository roomRepository;

    // write test cases here

    @Test
    public void givenRooms_whenGetRooms_thenReturnJsonArray()
            throws Exception {

        Room room1 = new Room(25, 5, "BoogyRoom", "BoogyRoom desc");
        Room room2 = new Room(26, 6, "SilentRoom", "SilentRoom desc");

        List<Room> allRooms = Arrays.asList(room1, room2);

        given(roomService.getAllRoom()).willReturn(allRooms);

        mvc.perform(get("/allRooms")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]").value(room1))
                .andExpect(jsonPath("$[1]").value(room2));
    }

    /**
     * Tests the functionality of the GET method on the '/hello' endpoind
     * Must return the message 'Hello world!'
     */
    @Test
    public void sayHello() {
        try {
            mvc.perform(MockMvcRequestBuilders.get("/hello"))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(MockMvcResultMatchers.status().isOk())
                    .andExpect(MockMvcResultMatchers.content().string("Hello World"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

}


