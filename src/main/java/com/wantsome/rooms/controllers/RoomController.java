package com.wantsome.rooms.controllers;


import com.wantsome.rooms.model.Room;
import com.wantsome.rooms.repository.RoomRepository;
import com.wantsome.rooms.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/*
Link: https://www.baeldung.com/spring-controller-vs-restcontroller
 */
@RestController
public class RoomController {

    //Logger logger = LoggerFactory.getLogger(RoomController.class);

    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomRepository roomRepository;

    // Request URI method
    @RequestMapping(value = "/hello")
    public String hello() {
        return roomService.hello();
    }


    @PutMapping(value = "/room", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public Room addRoom(@RequestBody Room room) {
        Room newRoom = roomService.addRoom(room);
        roomRepository.save(newRoom);
        return newRoom;
    }


    @PutMapping(value = "/multipleRooms", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<Room> addMultipleRooms(@RequestBody List<Room> roomListToAdd) {
        roomService.addMultipleRooms(roomListToAdd);
        roomRepository.saveAll(roomListToAdd);
        return roomListToAdd;
    }


    @GetMapping("/room/{id}")
    public Room getRoomById(@PathVariable String id) {
        return roomService.getRoomId(id);
    }


    @GetMapping("/allRooms")
    public Iterable<Room> getAllRoom() {
        return roomService.getAllRoom();
    }


    @PostMapping(value = "/updateRoom", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public Optional<Room> updateRoom(@RequestBody Room room) {
        roomRepository.save(room);
        return roomService.updateRoom(room);
    }


    @DeleteMapping("/room/{id}")
    public void deleteRoomById(@PathVariable String id) {
        roomRepository.deleteById(id);
        roomService.deleteRoomById(id);
    }

}
